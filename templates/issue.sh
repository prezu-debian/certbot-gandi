#!/bin/sh

. /srv/certbot/venv/bin/activate

certbot certonly --authenticator dns-gandi --dns-gandi-credentials /srv/certbot/{{ domain }}/config/gandi.ini \
  -d *.{{ domain }} \
  --config-dir /srv/certbot/{{ domain }}/config --work-dir /srv/certbot/{{ domain }}/workdir \
  --non-interactive --agree-tos --email {{ contact_email }}
