#!/bin/sh

. /srv/certbot/venv/bin/activate

certbot renew --config-dir /srv/certbot/{{ domain }}/config --work-dir /srv/certbot/{{ domain }}/workdir
